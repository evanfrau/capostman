# Capostman

This is a Postman project for the Automation Battle Guild

## What is Postman

Postman is an application used for API testing.

It is an HTTP client that tests HTTP requests, utilizing a graphical user interface, through which we obtain
different types of responses that need to be subsequently validated

<details>
<summary>Postman features</summary>

### Methods

Postman offers many endpoint interaction methods. The following are some of the most used, including their
functions:

- GET: Obtain information
- POST: Add information
- PUT: Replace/Update information
- DELETE: Delete information

### Response Codes

When testing APIs with Postman, we usually obtain different response codes. Some of the most common
include:

- 100 Series > Temporal responses, for example, "102 Processing"
- 200 Series > Responses where the client accepts the request and the server processes it successfully, for
  instance, "200 Ok"
- 300 Series > Responses related to URL redirection, for example, "301 Moved Permanently"
- 400 Series > Client error responses, for instance, "400 Bad Request"
- 500 Series > Server error responses, for example, "500 Internal Server Error"

### Collections

Postman gives the possibility to group different requests. This feature is known as "collections" and helps
organize tests.
These collections are folders where requests are stored and can be structured in whichever way the team
prefers. It is also possible to export-import them.

### Environments

Postman also allows us to create different environments through the generation/use of variables; for example,
a URL variable that is aimed towards different test environments (dev-QA), enabling us to execute tests in
different environments using existing requests

</details>

[Link to Postman documentation](https://learning.postman.com/docs/getting-started/introduction/)


## Assignment for the Automation Battle

We will use this sample API

https://restful-booker.herokuapp.com/apidoc/index.html

<details>
<summary>Easy Scenarios</summary>

- Create a post request to login and generate a token
- Create a post request to add a new booking
  - Test the status code
  - Test the header
  - Test response time
- Create a get request to view the new booking
- Create a put request to update the booking details
- Create a delete request to delete the booking
  - Verify the message in the response body
</details>

<details>
<summary>Medium Scenarios</summary>

- Create Environment variables such as Dev and QA to support the same test to execute on
  different environments
- Create 2 global variables username and password. Use these variables to login
</details>

<details>
<summary>Complex Scenarios</summary>

- Use any csv or json file as an input and create new bookings
- Execute any existing request that is created using Command Line and generate a report
  - Add data in Tests tab to make the report more presentative
</details>

## Install Postman

https://www.postman.com/downloads/

https://www.javatpoint.com/postman-installation-and-updates

## Import collection into Postman

Clone the project

```bash
git clone
```
Or [download ZIP](https://gitlab.com/evanfrau/capostman/-/archive/main/capostman-main.zip ) and unzip it

```
🗁 capostman
├── 🗎 automation-battle.postman_collection.json
├── 🗁 data
│   └── bookingData    
├── 🗁 env
│   ├── 🗎 automation-battle.postman_globals.json
│   ├── 🗎 DEVmeric.postman_environment.json
│   └── 🗎 QAnton.postman_environment.json
└── 🗁 results
```



### Configuration in Postman

1. Create a new workspace (optional if you have other collections)
2. Import `automation-battle.postman_collection.json` on _Collections_ tab
3. Import the 3 files from _env_ folder:

```
🗁 env
├── 🗎 automation-battle.postman_globals.json
├── 🗎 DEVmeric.postman_environment.json
└── 🗎 QAnton.postman_environment.json
```

4. Choose one of the environment DEVmeric or QAnton
5. Now you are ready to run the collection `Postman-automation-battle` ! (3 dots - run collection)

<img src="images/runwholecollection.PNG" width=600/>

6. And you can the results

<img src="images/resultsWholeCollection.PNG" width=400/>

### Run the complex scenario (With database)

1. Open the runner
2. Select `bookingData.json` from _data_ folder
3. Run it !
<img src="images/runnerWithData.png" width=600/>

4. You should see the results for multiple iterations
<img src="images/resultsData.png" width=600/>

## Reporting

We use [newman](https://github.com/postmanlabs/newman) & [newman-reporter-htmlextra](https://github.com/DannyDainton/newman-reporter-htmlextra) libraries to generate an html report

### Required

Install NodeJs

https://nodejs.org/en/download/

### Install the project from cloning this project

Clone the project...

```bash
git clone
```

Install the dependencies for reporting using package.json

```bash
npm install
```

Or install manually...

```bash
npm install newman

npm install newman-reporter-htmlextra
```

...then run _newman_ to see the report in _results_ folder
with the options for the _environment, data, html extra with custom title and file name_

- For QAnton environment:

```bash
newman run automation-battle.postman_collection.json -e ./env/QAnton.postman_environment.json -d ./data/bookingData.json -r htmlextra --reporter-htmlextra-title "QAnton battle report" --reporter-htmlextra-export ./results/QAnton-automation-battle-report.html
```

- For DEVmeric environment:

```bash
newman run automation-battle.postman_collection.json -e ./env/DEVmeric.postman_environment.json -d ./data/bookingData.json -r htmlextra --reporter-htmlextra-title "DEVmeric battle report" --reporter-htmlextra-export ./results/DEVmeric-automation-battle-report.html
```
----

## Bonus section - Run collection with Jenkins on AWS

In this section, you will learn how to 

<details>
<summary>Install Jenkins on AWS</summary>

You can follow all the detailed steps here:\
https://www.jenkins.io/doc/tutorials/tutorial-for-installing-jenkins-on-AWS/


<details><summary>Additional information:</summary> 

**For AWS IAM user, navigate IAM Users and create user:**
  
  1. Create group with admin access
  2. User in the group will inherit same access
  3. copy Access Key ID & Secret Key

**Configure Clouds > Amazon EC2:**
  
  1. Under Amazon EC2 Credentials, you can now add the keys via Jenkins Credientials Provider (AWS Credentials as Kind) 
  
  2. Under EC2 Key Pair's Private Key, copy the content of your private key
      - if you use openSSH, just copy the content from your generated file
      - if you use PuTTY, you first need to convert your ppk file to rsa

**Install Blue Ocean Plugin (optional):**
  
  1. Go to manage plugin
  2. Search for Blue Ocean in available tab
  3. Install & restart

</details>

</details>

<details>
<summary>Create a pipeline</summary>

**Prerequisite**

- Install Docker & git...

```JS
sudo su // to be root
yum install docker -y
yum install git
groupadd docker //create docker group if not exist
usermod -aG docker ${USER} // add user to docker group
chmod 666 /var/run/docker.sock // change docker.sock to new permission
systemctl restart docker // restart docker deamon service
```

- Install HTML Publisher plugin 
  - manage plugins > search for *HTML Publisher*
  - install and restart 

**Steps**
1. Create a new item
2. Enter a name and select Pipeline
3. Under pipeline > definition, select Pipele script from SCM
4. Select Git in SCM and enter this Git Repo URL (no need for credentials here as it is a public repo)
5. Under Branches to build, enter `*/main`
6. Enter `Jenkinsfile` in Script Path (file located in the root project)
7. Save and build now !

**Breakdown of the Jenkinsfile**

Project divided into mutliple stages: 

1. Clean workspace & checkout:
    1. Skip the default checkout (otherwise it will be clear after clean up)
    2. Clean workspace
    3. Then do the checkout

2. Run docker image `dannydainton/htmlextra` as it contains newman & htmlextra dependencies for reporting
    - added some options to also have results output in the console

3. Publish report with HTML Publisher plugin
    - By default the html in the workspace is set to remove Javascript, CSS,... for security reason
    --> Solution to see a beautiful report:
      ```bash
      System.setProperty("hudson.model.DirectoryBrowserSupport. CSP", "")
      ```
4. After executing all the stages, we do a cleanup of the workspace and remove unecessary folders and files
      - Using the [workspace cleanup plugin](https://plugins.jenkins.io/ws-cleanup/)
      ```JavaScript
          //We can specify the folders or files we want to be removed with type INCLUDE
          patterns: [[pattern: '**/images', type: 'INCLUDE'],
                    [pattern: 'README.md', type: 'INCLUDE']]
          ...
      ```

</details>

<details>
<summary>Schedule runs periodically</summary>

1. Go to the configuration of your pipeline
2. Enable `Build periodically`
3. Enter in Schedule your cron expressions

```Cron
// Run everyday at 6am & 6pm within the hour
H 6,18 * * *
```

</details>

<details>
<summary>Configure webhooks with GitLab</summary>

:warning: Doesn't work with your local Jenkins machine

1. Go to Manage Jenkins > Manage plugins
2. Install GitLab & Gitlab Hook
3. Restart
4. Go to your GitLab project
5. Go to Settings > Integrations
6. Select Jenkins
7. Enable & add the following:
    - [x] Enable Active
    - [x] Enable Push trigger
    - [x] Enter Jenkins server URL (your AWS Jenkins link)
    - [x] Enter the jenkins project name (pipeline job name)
    - [x] Enter the username & password for Jenkins
    - [x] Save changes
9. Go to Jenkins > configure of your pipeline
10. Under *Build Triggers* You should see a new checkbox `Build when a change is pushed to GitLab webhook URL: ...`
11. Enable it & Push Events (and any other options that you might need)
12. Do a change in your project and push it ! You should now see a new job running :runner:

</details>

<details>
<summary>Reporting</summary>

You can find the results :
 
 - in the Console Output of the run
 - After a successful run, under Workspaces folder:
    1. Click the link 
    2. Click on _newman_
    3. Click on the generated html report
  - You can download the zip file:
    - You should see the name of the report on the left navigation panel
</details>


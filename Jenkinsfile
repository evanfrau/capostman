pipeline {
    options {
        skipDefaultCheckout()
    }
    agent any
    stages {
        stage('Clean workspace') {
            steps {
                cleanWs()
            }
        }
        stage('Build') {
            steps {
                echo 'Building...'
                checkout scm
                sh '''
                docker run -t dannydainton/htmlextra run -h
                '''
            }
        }
        stage('Run collection') {
            steps {
                echo 'Testing...'
                sh '''
                docker run -v ${WORKSPACE}:/etc/newman --workdir /etc/newman \
                -t dannydainton/htmlextra run automation-battle.postman_collection.json \
                -e ./env/QAnton.postman_environment.json \
                -d ./data/bookingData.json \
                -r cli,htmlextra — reporter-htmlextra-export “newman/QAnton_report.html” --reporter-htmlextra-logs \
                --color off --disable-unicode
                '''
            }
        }
        stage('Publish report') {
            steps {
                echo 'Publishing...'
                publishHTML(target : [allowMissing: false,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: 'newman',
                    reportFiles: 'QAnton_report.html',
                    reportName: 'QAnton_report',
                    reportTitles: ''])
            }
        }
    }
    post {
        always {
            cleanWs(cleanWhenNotBuilt: false,
                    deleteDirs: true,
                    disableDeferredWipeout: true,
                    notFailBuild: true,
                    patterns: [[pattern: '.gitignore', type: 'INCLUDE'],
                               [pattern: '**/results', type: 'INCLUDE'],
                               [pattern: '**/images', type: 'INCLUDE'],
                               [pattern: 'package.json', type: 'INCLUDE'],
                               [pattern: 'README.md', type: 'INCLUDE']])
        }
    }
}
